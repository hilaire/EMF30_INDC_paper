generic_lineplot <- function (
  i_data,      # 
  i_filters,
  color="scenario",
  wrap = "",
  ncol = 4,
  xlab=NULL,
  ylab=NULL,
  title=NULL,
  save=""
  ){
  
  datatmp = i_data
  
  for (kvar in names(i_filters)) {
    if (!kvar %in% names(datatmp)) stop("Error: column name not existent in data frame.")
    datatmp = datatmp[which(datatmp[[kvar]] %in% i_filters[[kvar]]),]
  }
  
  p = ggplot(data=datatmp) +
    geom_line(aes_string(x="period", y="value", color=color))
  if (wrap!="") {
    p = p + facet_wrap(wrap, ncol=ncol)
  }
  p = p +
    theme_bw() +
    theme(legend.position="bottom")
  
  if (!is.null(xlab)) p = p + xlab(xlab)
  if (!is.null(ylab)) p = p + ylab(ylab)
  if (!is.null(title)) p = p + ggtitle(title)
  
  print(p)
  
  if (save!="") {
    ggsave(filename = save, width=16,height=12)
  }
}